import React, { useEffect, useState } from "react";
import { useAddPersonMutation } from "../features/PersonAPI";

export interface Person {
    id?:number,
    name:string,
    firstname:string,
    age:number
}

export default function AddPerson() {
    const [AddOne,{data, error, isLoading}] = useAddPersonMutation();
    const [person, setPerson] = useState<Person>({
        name:'',
        firstname:'',
        age:0
    });

    const addPerson = () => {
        AddOne(person)
    }

    useEffect(()=>{
        console.log(data);
        
    }, [data])

    return (
        <section>
            <h1>Add Person</h1>

            <form>
                <div className="form-group">
                    <label htmlFor="firstname">Firstname</label>
                    <input type="text" className="form-control" id="firstname" value={person.firstname} />

                </div>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" id="name" value={person.name} />
                </div>
                <div className="form-group">
                    <label htmlFor="age">Age</label>
                    <input type="number" className="form-control" id="age" value={person.age} />
                </div>
                <button onClick={addPerson} type="submit" className="btn btn-primary">Submit</button>
            </form>

        </section>
    )
}
