import React, { useEffect, useState } from "react";
import { useGetAllPersonQuery } from "../features/PersonAPI";
import { Person } from "./AddPerson";


export default function Home() {
    const [persons, setPersons] = useState<Person[]>([]);
    const [selected, setSelected] = useState<any>(null);
    const {data, error, isLoading}=useGetAllPersonQuery();
    useEffect(() => {
        if (data) {
            setPersons(data)
            
        }
    }, [data]);

    
    return (
        <section>
            <h1>Persons List</h1>
            {JSON.stringify(selected)}
            <div className="list-group">
                {!isLoading && persons.map(person => 
                    <button key={person.id} onClick={() => setSelected(person)} className="list-group-item">{person.name} {person.firstname}</button>
                )}
            </div>
            
            {selected && 
            <div className="modal" tabIndex={-1} role="dialog" style={{display:'block'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{selected.name}</h5>
                        <button type="button" className="btn-close" onClick={() => setSelected(null)} >
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>Firstname : {selected.firstname}</p>
                        {selected.age != -1 ? <p>Age : {selected.age}</p> : <p>Deceased</p>}
                    </div>
                    </div>
                </div>
            </div>
            }
        </section>
    )
}
