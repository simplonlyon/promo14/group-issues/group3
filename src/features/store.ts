import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import { PersonApi } from './PersonAPI';



export const store = configureStore({
    reducer: {
        [PersonApi.reducerPath]: PersonApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(PersonApi.middleware),
});


setupListeners(store.dispatch)