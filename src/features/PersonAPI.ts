import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { Person } from '../pages/AddPerson'

export const PersonApi = createApi({
    reducerPath: "PersonAPI",
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:4000/' }),
    endpoints: (build) => ({
        getAllPerson: build.query<Person[], void>({
            query:() => `person`
        }),
        addPerson: build.mutation<Person, Person>({
            query:(person) => ({ url: `person`, method: 'POST', body: person }), 
        })

    }),
})

export const { useGetAllPersonQuery, useAddPersonMutation } = PersonApi